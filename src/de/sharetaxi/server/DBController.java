package de.sharetaxi.server;

import java.io.FileNotFoundException;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class DBController {

	private static String DB_URL = "localhost";
	private static String DB_NAME = "ShareTaxi";
	private static String DB_USER = "root";
	private static String DB_PASSWORD = "";
	
	
	/**
	 * The database connection
	 */
	private Connection conn;
	
	/**
	 * The file scanner
	 */
	private Scanner scanner;
	
	public DBController() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception ex) {
            // handle the error
        }
		
		try {
			conn = DriverManager.getConnection("jdbc:mysql://" + DB_URL + "/" + DB_NAME + "?"
		              + "user=" + DB_USER + "&password=" + DB_PASSWORD);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	public ArrayList<Taxi> getTaxiList(LocalDateTime now, float pickup_lo, float pickup_la, float dropoff_lo, float dropoff_la) {
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery("SELECT * FROM `demo` WHERE `pickup_datetime` > '" + now.format(TaxiServer.FORMATTER) + "' AND `pickup_datetime` < '" + now.plusMinutes(30).format(TaxiServer.FORMATTER) + "';");
			
			ArrayList<Taxi> out = new ArrayList<>();
			while(result.next() )
				out.add(new Taxi(result));

			
			return out;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			e.getSQLState();
		}
			
		return null;
	}

	/**
	 * Drop table and recreate
	 */
	@SuppressWarnings("unused")
	private void resetTables() {
		
		try {
			Statement stmt = conn.createStatement();
		
			// Delete old Tables
			stmt.executeUpdate("DROP TABLE IF EXISTS demo");
			
			// Create Tables
			String query = "CREATE TABLE IF NOT EXISTS `demo` (" +
			  "`ID` int(11) NOT NULL AUTO_INCREMENT," +
			  "`pickup_datetime` datetime NOT NULL," +
			  "`trip_time_in_secs` int(11) NOT NULL," +
			  "`trip_distance` float NOT NULL," +
			  "`pickup_longitude` float NOT NULL," +
			  "`pickup_latitude` float NOT NULL," +
			  "`dropoff_longitude` float NOT NULL," +
			  "`dropoff_latitude` float NOT NULL," +
			  "`fare_amount` float NOT NULL," +
			  "PRIMARY KEY (`ID`) " +
				") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			stmt.executeUpdate(query);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Fill table with random values
	 * @param offset	the first random value
	 * @param count		amount of random values
	 */
	@SuppressWarnings("unused")
	private void insertValues(String path) {
		
		// Init Scanner
		if (scanner == null) {
			try {
				scanner = new Scanner(new File(path));
				scanner.useDelimiter("[\\n|,]+");
				scanner.useLocale(Locale.US);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
				
		// Fill table with demo values
		try {
			// Turn autocommit off
			conn.setAutoCommit(false);
			int count = 1000;
			// Insert values
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO demo VALUES (default, ?, ?, ?, ?, ?, ?, ?, ?)");
			int i = 0;
			while (scanner.hasNext()) {
				scanner.next();
				scanner.next();
				stmt.setString(1, scanner.next());
				scanner.next();
				stmt.setInt(2, scanner.nextInt());
				stmt.setFloat(3, scanner.nextFloat());
				stmt.setFloat(4, scanner.nextFloat());
				stmt.setFloat(5, scanner.nextFloat());
				stmt.setFloat(6, scanner.nextFloat());
				stmt.setFloat(7, scanner.nextFloat());
				scanner.next();
				stmt.setFloat(8, scanner.nextFloat());
				scanner.nextLine();
				stmt.addBatch();
				
				if (i % 2500 == 0) {
					System.out.println(i);
					stmt.executeBatch();
					stmt.clearBatch();
				}
				i++;
			}
			
			stmt.executeBatch();
			stmt.close();
			
			// Turn autocommit on
			conn.commit();
			conn.setAutoCommit(true);
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getErrorCode() + " " + e.getSQLState());
		}
	}
	
	/**
	 * Close DB connection
	 */
	public void close() {
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * Main to fill database with taxi data
	 * @param args
	 */
	public static void main(String[] args) {
		
		DBController c = new DBController();
		c.resetTables();
		c.insertValues("data/sorted_data.csv");
		
	}
	
	
}
