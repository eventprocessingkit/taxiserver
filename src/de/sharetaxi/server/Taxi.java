package de.sharetaxi.server;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

/**
 * Created by micro on 10.06.2016.
 */
public class Taxi implements Comparable<Taxi>{

	public int ID; 
    public LocalDateTime pickup_datetime;
    public int trip_time_in_seconds;
    public float trip_distance;
    public float pickup_longitude;
    public float pickup_latitude;
    public float dropoff_longitude;
    public float dropoff_latitude;
    public float price;
    
    private double pickupDistance;
    private double dropOffDistance;
    
    public Taxi(ResultSet r) throws SQLException {

    	this.ID = r.getInt("ID");
    	this.pickup_datetime = r.getTimestamp("pickup_datetime").toLocalDateTime();
    	this.trip_time_in_seconds = r.getInt("trip_time_in_secs");
    	this.trip_distance = r.getFloat("trip_distance");
    	this.pickup_longitude = r.getFloat("pickup_longitude");
    	this.pickup_latitude = r.getFloat("pickup_latitude");
    	this.dropoff_longitude = r.getFloat("dropoff_longitude");
    	this.dropoff_latitude = r.getFloat("dropoff_latitude");
    	this.price = r.getFloat("fare_amount");
    }
    
    public void calculateWalkDistances(float startLat, float startLon, float targetLat, float targetLon) {
    	this.pickupDistance = DistanceCalculator.distance(startLat, startLon, pickup_latitude, pickup_longitude, "M");
    	this.dropOffDistance = DistanceCalculator.distance(targetLat, targetLon, dropoff_latitude, dropoff_longitude, "M");
    }
    
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ID + "," + pickup_datetime.format(TaxiServer.FORMATTER) + "," + trip_time_in_seconds + "," + trip_distance + "," + pickup_longitude
				+ "," + pickup_latitude + "," + dropoff_longitude + "," + dropoff_latitude;
	}

	public double getDropOffDistance() {
		return dropOffDistance;
	}

	public double getPickupDistance() {
		return pickupDistance;
	}
	
	public double getWalkDistance() {
		return getPickupDistance() + getDropOffDistance();
	}


	@Override
	public int compareTo(Taxi t) {
		return pickup_datetime.compareTo(t.pickup_datetime);
		//return Double.compare(getPickupDistance(), t.getPickupDistance());
	}
	
}
