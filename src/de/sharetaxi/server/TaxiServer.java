package de.sharetaxi.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class TaxiServer {

	/**
	 * Limit taxis send to server
	 */
	public static int MAX_TAXIS = 10;
	
	/**
	 * Set max distances for taxis to select
	 */
	public static double MAX_DROPPFF_DISTANCE = 0.5;
	
	/**
	 * Port listening on
	 */
	public static int PORT = 9090;
	
	/**
	 * Server version
	 */
	public static int VERSION_MAJOR = 1;
	public static int VERSION_MINOR = 0;
	public static int VERSION_FIX = 0;

	/**
	 * Date and Time format
	 */
	public static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
	// Random instance
	private static Random random = new Random();
	
	/**
	 * Main
	 */
	public static void main(String[] args) throws IOException{

		System.out.println("Starting TaxiServer v. " + VERSION_MAJOR + "." + VERSION_MINOR + "." + VERSION_FIX);
		
		// Init DB connection
		DBController db = new DBController();
		System.out.println("Database connection established");
		
		// Listen on PORT
		ServerSocket listener;
		try {
			listener = new ServerSocket(PORT);
			System.out.println("Server started, listening on port " + PORT);
			System.out.println("=========");
		} catch (BindException e) {
			System.err.println("Can't open socket. Port " + PORT + " already in use");
			System.err.println("Server shut down");
			db.close();
			return;
		}
        
		try {
        	// Main loop
            while (true) {
            	// New connection
                Socket socket = listener.accept();
                System.out.println("New connection");
                
                // Init readers
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                try {
                	String inMsg = in.readLine();
                	String[] query = inMsg.split(",");
                	
                	if (query.length != 6) {
                		System.err.println("Invalid input msg: '" + inMsg + "'");
                		continue;
                	}
                	
                	// Get taxis in time frame (now - now+30min) 
                	ArrayList<Taxi> taxiList = db.getTaxiList(LocalDateTime.parse(query[0], FORMATTER), 0, 0, 0, 0);
                	
                	// Create response message
                	String outMsg = createMessage(query, taxiList);
                	
                	// Send response
                    out.print(outMsg);
                    System.out.println("Sent available taxis");
                } finally {
                	// Close connection
                	out.close();
                	in.close();
                    socket.close();
                    System.out.println("Connection closed");
                }
            }
        }
        finally {
            listener.close();
            db.close();
        }
	}
	
	/**
	 * Creates response message from taxiList and query
	 * @param query query data
	 * @param taxiList list of taxis in timeFrame
	 * @return response message
	 */
	private static String createMessage(String[] query, ArrayList<Taxi> taxiList) {
		
		String out = "";
		float maxPickupDistance = Float.parseFloat(query[1]);
		
		ArrayList<Taxi> selectedTaxis = new ArrayList<>();
		int count = 0;
		for(Taxi taxi : taxiList) {
			
			// Calculate walk distances
			taxi.calculateWalkDistances(Float.parseFloat(query[2]), Float.parseFloat(query[3]), Float.parseFloat(query[4]), Float.parseFloat(query[5]));
			
			// Check for distance to walk
			if (taxi.getPickupDistance() <= maxPickupDistance && taxi.getDropOffDistance() <= MAX_DROPPFF_DISTANCE) { 
				selectedTaxis.add(taxi);

				count++;
			}
			
			// MAX_TAXIS reached
			if (count >= MAX_TAXIS)
				break;
		}
		
		// Sort (walk distance)
		Collections.sort(selectedTaxis);
		
		// Create output message form selected Taxis
		for (int i = 0; i < Math.min(selectedTaxis.size(), MAX_TAXIS); i++)
			out += taxiToString(selectedTaxis.get(i)) + "\n";
		
		System.out.println("Packed " + count + " taxis");
		
		return out;
	}

	/**
	 * Creates the comma separated message for a taxi
	 * @param t the Taxi
	 * @return the message
	 */
	private static String taxiToString(Taxi t) {
		String out = "";
		
		// Start time
		out += t.pickup_datetime.format(FORMATTER) + ",";
		
		// Seats
		out += (random.nextInt(3) + 1) + ",";
		
		// Normal price
		float normalPrice =  t.price;
		out += normalPrice + ",";
		
		// Actual price
		out += (normalPrice * 0.8) + ",";
		
		// Taxi start
		out += t.pickup_latitude + "," + t.pickup_longitude + ",";
		
		// Taxi end
		out += t.dropoff_latitude + "," + t.dropoff_longitude;
				
		return out;
	}

}
