Server zur Buchung von Shared-Taxen
===============================

Dieses Repository ist im Rahmen des Seminars "Event Processing" des
Forschungszentrum Informatik (FZI) im Sommersemster 2016 am 
Karlsruher Institut für Technologie (KIT) entstanden.

Von Tim Bossenmaier, Nevena Nikolajevic und Adrian Vetter.


### Verwendung
1. SQL Datenbank aufsetzen.
2. Eine neue leere Datenbank anlegen.
3. Eclipse öffnen.
4. Per **File** -> **Import...** -> **Git** -> **Projects from Git** das Projekt importieren.
5. Datensatz von http://www.debs2015.org/call-grand-challenge.html herunterladen.
6. _sorted_data.csv_ in das Verzeichnis _data_ legen (falls nicht vorhanden neu anlegen).
7. In _DBController.java_ die Verbindungsdaten zur Datenbank anpassen.
8. Main-Methode in _DBController.java_ ausführen um Datenbank zu füllen.
9. Server mit der Main-Methode aus _TaxiServer.java_ starten.
10. Per [Android App](https://bitbucket.org/eventprocessingkit/sharetaxi) kann nun auf den Server zugegriffen werden.